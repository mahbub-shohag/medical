package com.discharge.medical.dischargecertificate;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/discharge")
public class DischargeController {
	@GetMapping("/index")
	public String index() {
		return "Welcome To Our Application";
	}
}
